package com.zeitheron.hammercore.tile.tooltip;

public enum eNumberFormat
{
	FULL, COMPACT, COMMAS, NONE;
}