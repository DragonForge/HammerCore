package com.zeitheron.hammercore.client.utils.gl.shading;

import net.minecraftforge.fml.common.eventhandler.Event;

/**
 * Called to create shaders within HammerLib's shading pipeline.
 */
public class InitializeShadersEvent
		extends Event
{
}