package com.zeitheron.hammercore.api.blocks;

import net.minecraft.item.Item;

public interface IBlockItemRegisterListener
{
	void onItemBlockRegistered(Item item);
}